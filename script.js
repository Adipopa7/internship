// Function that changes the class of the myDropdown element
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('#srch-term')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {  
        openDropdown.classList.remove('show');
      }
    }
  }
}
//Fixer for nav on mobile size
var clickedOnTheHamburgerMenu = false;
function wishAndCartPositionFixer(){
  if(clickedOnTheHamburgerMenu == false){
    clickedOnTheHamburgerMenu = true;
    document.getElementById("wish-and-cart").style.marginTop = "-20.8em";
  } else if (clickedOnTheHamburgerMenu == true){
    clickedOnTheHamburgerMenu = false;
    document.getElementById("wish-and-cart").style.marginTop = "-2.5em";
  }
}

//Hover state for products
function showActionButtons(product){
    $('.'+ product + ' .title').hide();
    $('.'+ product + ' .fonts').show();
}
function hideActionButtons(product){
    $('.'+ product + ' .fonts').hide();
    $('.'+ product + ' .title-fonts .title').show();

}

//Text highlight for carousel
function textHighlight(){
    setInterval(function () {
        $('.highlight-white').removeClass('highlight-white');
        if($('#carousel-indicator-1').hasClass('active')){
            $('.text1').addClass('highlight-white');
        } else if($('#carousel-indicator-2').hasClass('active')){
            $('.text2').addClass('highlight-white');
        } else {
            $('.text3').addClass('highlight-white');
        }
    }, 200);
}


/* BLOG NEWS */
var blogNrFirstBlog = 1;
var blog1Title = 'Nice & Clean. The best for your blog!';
var blog1Text = "Vivamus metus turpis, bibendum vitae euismod vel, vulputate vel";
var blog1Date = 'APR' + '<br>' + '01';

var blog2Title = "Lorem ipsum dolor sit amet!";
var blog2Text = "Suspendisse porta tellus in libero aliquet maximus. Donec ultrices purus";
var blog2Date = 'DEC'  + '<br>' + '25';

var blogNrSecondBlog = 3;
var blog3Title = 'What an Ecommerce theme!';
var blog3Text = 'Sed vitae dolor dui. Morbi ligula turpis, commodo vitae';
var blog3Date = 'APR' + '<br>' + '01';

var blog4Title = 'Nam efficitur felis ut urna fermentum';
var blog4Text = ' Nunc vitae scelerisque massa. Quisque dictum';
var blog4Date = 'IAN' + '<br>' + '27';
var selectors1 = '.blog-entry-1 h5, .blog-entry-1 .date, .blog-entry-1 p';
var selectors2 = '.blog-entry-2 h5, .blog-entry-2 .date, .blog-entry-2 p';

function blogChanger(){

    setInterval(function(){
        if(blogNrFirstBlog == 1){
            $(selectors1).fadeOut();
            document.getElementById('date').innerHTML = blog2Date;
            $('.blog-entry-1 h5').text(blog2Title);
            $('.blog-entry-1 p').text(blog2Text);
            $(selectors1).fadeIn();
            blogNrFirstBlog = 2;
        } else {
            $(selectors1).fadeOut();
            document.getElementById('date').innerHTML = blog1Date;
            $('.blog-entry-1 h5').text(blog1Title);
            $('.blog-entry-1 p').text(blog1Text);
            $(selectors1).fadeIn();
            blogNrFirstBlog = 1;
        }
        if(blogNrSecondBlog == 3){
            $(selectors2).fadeOut();
            document.getElementById('date2').innerHTML = blog4Date;
            $('.blog-entry-2 h5').text(blog4Title);
            $('.blog-entry-2 p').text(blog4Text);
            $(selectors2).fadeIn();
            blogNrSecondBlog = 4;
        } else {
            $(selectors2).fadeOut();
            document.getElementById('date2').innerHTML = blog3Date;
            $('.blog-entry-2 h5').text(blog3Title);
            $('.blog-entry-2 p').text(blog3Text);
            $(selectors2).fadeIn();
            blogNrSecondBlog = 3;
        }
    }, 5000);
}

/* TWITTER ---*/
var tweetNr = 1;
function tweetChanger(){
    setInterval(function(){
        if(tweetNr == 1){
            $('.twitter-widget-1').fadeOut();
            $('.twitter-widget-2').fadeIn();
            tweetNr = 2;
        } else {
            $('.twitter-widget-2').fadeOut();
            $('.twitter-widget-1').fadeIn();
            tweetNr = 1;
        }
    }, 5000);
}

/* NEWSLETTER-------- */

var data = [];
data[0] = {email: 'youremail@yahoo.com'};
function saveEmail(){
    var email = document.getElementById('email').value;
    data.push(email);
    localStorage.setItem('data', JSON.stringify(data));
    var retrievedEmail = localStorage.getItem('data');
    console.log('retrievedEmail: '  + JSON.stringify(retrievedEmail));
    document.getElementById('email').value = '';
}

/* PRODUCT VIEW */
function listView(){
    $('.new-arrivals').find('img').css('display', 'block');
    $('#gridview').css('border-color', 'initial');
    $('#listview').css('border-color', 'black');

    $('.product1').find('.title-fonts').css({'top' : '11.5em', 'left' : '5em'});
    $('.product4').find('.title-fonts').css({'top' : '11.5em', 'left' : '5em'});


    $('.product2').find('.title-fonts').css({'top' : '25em', 'left' : '5em'});
    $('.product5').find('.title-fonts').css({'top' : '25em', 'left' : '5em'});

    $('.product3').find('.title-fonts').css({'top' : '38.5em', 'left' : '5em'});
    $('.product6').find('.title-fonts').css({'top' : '38.5em', 'left' : '5em'});
}

function gridView(){
    $('.new-arrivals').find('img').css('display', 'inline');
    $('#listview').css('border-color', 'initial');
    $('#gridview').css('border-color', 'black');

    $('.product1').find('.title-fonts').css({'top' : '10.7em', 'left' : '5.5em'});
    $('.product4').find('.title-fonts').css({'top' : '10.7em', 'left' : '5.5em'});


    $('.product2').find('.title-fonts').css({'top' : '10.7em', 'left' : '20.2em'});
    $('.product5').find('.title-fonts').css({'top' : '10.7em', 'left' : '20.2em'});

    $('.product3').find('.title-fonts').css({'top' : '10.7em', 'left' : 'initial'});
    $('.product6').find('.title-fonts').css({'top' : '10.7em', 'left' : 'initial'});
     if ($(window).width() < 960){
        $('.product1').find('.title-fonts').css({'top' : '10em', 'left' : '10.5em'});
        $('.product4').find('.title-fonts').css({'top' : '10em', 'left' : '10.5em'});


        $('.product2').find('.title-fonts').css({'top' : '10em', 'left' : '25em'});
        $('.product5').find('.title-fonts').css({'top' : '10em', 'left' : '25em'});

        $('.product3').find('.title-fonts').css({'top' : '10em', 'right' : '5.5em'});
        $('.product6').find('.title-fonts').css({'top' : '10em', 'right' : '5.5em'});
    }
}

